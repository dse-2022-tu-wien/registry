# How to deploy DSE

```sh
helm repo add dse https://gitlab.com/api/v4/projects/36830669/packages/helm/stable

helm repo update

#optional: helm search repo dse --devel --versions

helm upgrade --install dse-deployment \
    --namespace dse-deployment \
    --create-namespace  \
    --wait \
    --values values.yaml \
    dse/dse
```

## values.yaml

```yaml
certs-issuer:
  email: "YOUR_EMAIL@example.com"
  server: https://acme-staging-v02.api.letsencrypt.org/directory

frontend:
  ingress:
    enabled: true

mongodb-sharded:
  global:
    storageclass: "local-path"
  auth:
    rootPassword: Undocked-Dragster9-Overtly-Showplace-Blanching
```

## container registry

Here you can find all our containers: [https://gitlab.com/dse-2022-tu-wien/registry/container_registry](https://gitlab.com/dse-2022-tu-wien/registry/container_registry)
